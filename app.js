function filterBy(arr, dataType) {
    let filteredArray = arr.filter(function (item) {
        return typeof(item) !== dataType;
    });
    return filteredArray;
}
Args =['hello', 'world', 23, '23', null];
console.log(filterBy(Args, 'string'));